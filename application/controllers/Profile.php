<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// only login users can access Account controller
		//$this->verify_login();
		$this->load->model('user_model');
	}

	public function index($username){
		//echo urlencode(url_title($username)); 
		$username = urldecode($username);
		$datasl = $this->user_model->get_user_info($username);		
		$category = $this->user_model->get_category($datasl);

		/*$sql = "SELECT a.username, a.email, a.phone, a.created_on, b.* FROM users a LEFT JOIN business_profiles b ON b.user_id = a.id WHERE a.username = '$username'";
		$q = $this->db->query($sql);
		$this->mViewData['numofRow'] = $q->num_rows();
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
		}*/
				
		$this->mPageTitle = $username;				
		//$this->render('pages/search', 'default');
		$this->mViewData['datasl'] = $datasl;
		$this->mViewData['category'] = $category;

		if ($this->ion_auth->logged_in()){
			$this->render('pages/profileview', 'full_width');
		}else{	
			$this->render('pages/userview', 'full_width');
		}
	}
	
}