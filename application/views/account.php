
<h3>Welcome, <?php if($user->first_name != '') {echo $user->first_name.' '.$user->last_name;} else { echo $user->email;} ?>!</h3>

<a href="auth/logout" class="btn">Logout</a>


<?php /*?><div id="wrapper7">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h1>My Account</h1>
        </div>
        <div class="account-box">
          <?php print_r($zones); $user_id = $userDetalis->id;?> 
          <?php if($this->session->flashdata('success') != ''){?><div class="col-md-12"><div class="alert alert-success" role="alert"><?php echo @$this->session->flashdata('success');?></div></div><?php }?>
          <?php $attributes = array('class' => 'form-horizontal forms3', 'enctype'=>'multipart/form-data'); echo form_open("user/view/$user_id", $attributes);?>
          <div class="col-md-4">
          	<div class="sidebar_account">           
            <h2>Other Information</h2>
            <hr />
            <ul>
            	<li><a href="<?= base_url();?>auth/change_email/">Change Email</a></li>
                <li><a href="<?= base_url();?>auth/change_password/">Change Password</a></li>
                <li><a href="<?= base_url();?>auth/unregister/">Unregister</a></li>
                <li><a href="<?= base_url();?>auth/logout/">Logout</a></li>
            </ul>
            </div>
          </div>
          <div class="col-md-7 white_background">
          <?php //echo "<pre>";print_r($userDetalis); echo "</pre>";?>
          <?php if($this->session->flashdata('message') != ''){?><br /><div class="alert alert-danger" role="alert"><?php echo @$this->session->flashdata('message');?><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><?php }?>
          <h2>Hello <?php if($user->first_name != '') {echo $user->first_name.' '.$user->last_name;} else { echo $user->email;} ?>, <small>Update your profile</small></h2>
          <hr />
                                 
              <div class="form-group <?php if(form_error('firstname') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('First Name', $firstname['id'], array('class'=>'label_class required')); ?>	
                  <?php echo form_input($firstname);?>
                  <?php echo form_error('firstname'); ?>
                </div>
              </div>

              <div class="form-group <?php if(form_error('lastname') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Last Name', $lastname['id'], array('class'=>'label_class required')); ?>	
                  <?php echo form_input($lastname);?>
                  <?php echo form_error('lastname'); ?>
                </div>
              </div>
              
              <div class="form-group">                
                <div class="col-md-7">
                  <label class="label_class">EMAIL ADDRESS</label>
                  <input class="form-control" type="text" readonly="readonly" value="<?= $userDetalis->email;?>">
                </div>
              </div>

              <div class="form-group <?php if(form_error('phonenumber') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Phone Number', $phonenumber['id'], array('class'=>'label_class required')); ?>	
                  <?php echo form_input($phonenumber);?>
                  <?php echo form_error('phonenumber'); ?>
                </div>
              </div>

              <div class="form-group <?php if(form_error('linkedin_url') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('LinkedIn Url', $linkedin_url['id'], array('class'=>'label_class')); ?>
                  <?php echo form_input($linkedin_url);?>
                  <?php echo form_error('linkedin_url'); ?>
                </div>
              </div>

              <div class="form-group <?php if(form_error('twitter_url') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Twitter Url', $twitter_url['id'], array('class'=>'label_class')); ?>
                  <?php echo form_input($twitter_url);?>
                  <?php echo form_error('twitter_url'); ?>
                </div>
              </div>

              <div class="form-group <?php if(form_error('github_url') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Github Url', $github_url['id'], array('class'=>'label_class')); ?>
                  <?php echo form_input($github_url);?>
                  <?php echo form_error('github_url'); ?>
                </div>
              </div>
              
              <div class="form-group <?php if(form_error('skype_name') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Skype Name', $skype_name['id'], array('class'=>'label_class')); ?>
                  <?php echo form_input($skype_name);?>
                  <?php echo form_error('skype_name'); ?>
                </div>
              </div>

              <div class="form-group <?php if(form_error('dribble_url') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Dribbble Url', $dribble_url['id'], array('class'=>'label_class')); ?>
                  <?php echo form_input($dribble_url);?>
                  <?php echo form_error('dribble_url'); ?>
                </div>
              </div>

              <div class="form-group <?php if(form_error('codecademy_url') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">                  
                  <?php echo form_label('Codecademy Url', $codecademy_url['id'], array('class'=>'label_class')); ?>
                  <?php echo form_input($codecademy_url);?>
                  <?php echo form_error('codecademy_url'); ?>
                </div>
              </div>

              <hr>

              <div class="form-group <?php if(form_error('time_zone') != ''){?>has-error<?php } ?>">                                
                <div class="col-md-7">
                  <?php echo form_label('Time Zone', 'time_zone', array('class'=>'label_class required')); ?>
                  <div class="ui-select">
                    <?php echo form_dropdown('time_zone', $zones, $userDetail->time_zone, array('class'=>'form-control', 'id'=>'time_zone'));?>
                    <?php echo form_error('time_zone'); ?>
                  </div>
                </div>
              </div>            

              <?php /*?><div class="form-group <?php if(form_error('profile_status') != ''){?>has-error<?php } ?>">                
                <label class="col-sm-4 label_class"></label>
                <div class="col-sm-8">
                  <div class="checkbox">
                    <label>Make Profile Private?<?php
                      if($userDetail->profile_status == 1){
                        echo '<input type="checkbox" name="profile_status" checked="checked"/>';
                            }
                      else {
                        echo '<input type="checkbox" name="profile_status"/>';
                      }
                    ?></label>
                  </div>
                </div>
              </div><?php ?>  

              <hr>
              <div class="form-group">                
                <div class="col-sm-8">
                  <?php echo form_label('BIO', 'bio', array('class'=>'label_class')); ?>                                  
                  <?php echo form_textarea($bio);?>
                  <?php echo form_error('bio'); ?>
                </div>
              </div>
              <hr>

              <div class="form-group">                
                <div class="col-sm-8 emailcheck">
                  <p>Email notifications</p>
                </div>
                <label class="col-sm-4 label_class"></label>
                <div class="col-sm-8">
                  <div class="emailcheck">                    
                    <div class="">
                      <label><input type="checkbox" name="enotification" checked="checked" /> SEND NEW MESSAGE EMAILS                      
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div class="form-group">
              <div class="col-sm-8">
                  <div class="accoutn-img">
                    <!--INPUT-->
                   <?php $imageUrl = base_url()."uploads/profile/".$userDetail->profile_img_path; 
                   if($userDetail->profile_img_path != ''){
                   ?>
                   <img src="<?= $imageUrl; ?>" class="img-responsive"><br />
                   <input name = "profile_img_path" type="file" class="input-xlarge" id = "profile_img_path" /> 
                   <?php }else{ ?>
                   <input name = "profile_img_path" type="file" class="input-xlarge" id = "profile_img_path" /> 
                   <?php } ?>
                   <?php echo form_error('profile_img_path'); ?>
                  </div>
              </div>
              </div>
              <hr />
              
              <div class="form-group">
                <div class="col-md-7"><input type="submit" value="Update Account" class="btn btn-success"></div>
              </div>
            
          </div>
          <div class="clearfix"></div>
          <?php echo form_close();?>
        </div>        
      </div>      
    </div>
  </div>
</div>
<?php */?>