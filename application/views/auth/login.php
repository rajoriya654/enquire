<div class="page-header"><h1><?php echo $page_title; ?></h1><p>Please log in to continue.</p></div>
<?php 
	$email  = '';
	if(isset($_SESSION['form-1'])){ 
		$email = $_SESSION['form-1']['email']; 
	}
?>
<?php echo $form->open(); ?>

	<?php echo $form->messages(); ?>
	
	<?php echo $form->bs3_email('Email', 'email', $email); ?>
	<?php echo $form->bs3_password('Password'); ?>

	<div class="">
		<label>
			<input type="checkbox" name="remember"> Remember me
		</label>
	</div>
	<div class="form-group">
		Don't have an account? <a href="auth/sign_up">Sign Up</a>
	</div>
	<div class="form-group">
		<a href="auth/forgot_password">Forgot password?</a>
	</div>
	<?php echo $form->bs3_submit('Log in'); ?>

<?php echo $form->close(); ?>