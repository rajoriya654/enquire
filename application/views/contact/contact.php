<div class="page-header"><h1>Have a question?<?php //echo $page_title; ?></h1><p>PLEASE FILL OUT THE FORM TO CONTACT US.</p></div>
<?php echo $this->session->flashdata('msg'); ?>
<?php echo $form->open(); ?>
	<?php echo $form->messages(); ?>
	<?php echo $form->bs3_text('Name', 'name', '', array('placeholder'=>'Your Full Name')); ?>	
	<?php echo $form->bs3_email('Email address', 'email', '', array('placeholder'=>'Email address')); ?>
	<?php echo $form->bs3_text('Subject', 'subject', '', array('placeholder'=>'Your Subject')); ?>
	<?php echo $form->bs3_textarea('Message', 'message', '', array('placeholder'=>'Your Message')); ?>
	<p><?php echo $form->field_recaptcha(); ?></p>

	
	<?php echo $form->bs3_submit('submit'); ?>   

<?php echo $form->close(); ?>
            <?php /*?><?php echo $this->session->flashdata('msg'); ?>
<?php $attributes = array("class" => "form-horizontal", "name" => "contactform");
            echo form_open("contact/index", $attributes);?>
            <fieldset>
            <legend>Contact Form</legend>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="name" class="control-label">Name</label>
                </div>
                <div class="col-md-12">
                    <input class="form-control" name="name" placeholder="Your Full Name" type="text" value="<?php echo set_value('name'); ?>" />
                    <span class="text-danger"><?php echo form_error('name'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="email" class="control-label">Email ID</label>
                </div>
                <div class="col-md-12">
                    <input class="form-control" name="email" placeholder="Your Email ID" type="text" value="<?php echo set_value('email'); ?>" />
                    <span class="text-danger"><?php echo form_error('email'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="subject" class="control-label">Subject</label>
                </div>
                <div class="col-md-12">
                    <input class="form-control" name="subject" placeholder="Your Subject" type="text" value="<?php echo set_value('subject'); ?>" />
                    <span class="text-danger"><?php echo form_error('subject'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="message" class="control-label">Message</label>
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" name="message" rows="4" placeholder="Your Message"><?php echo set_value('message'); ?></textarea>
                    <span class="text-danger"><?php echo form_error('message'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <input name="submit" type="submit" class="btn btn-primary" value="Send" />
                </div>
            </div>
            </fieldset>
            <?php echo form_close(); ?>
<?php */?>