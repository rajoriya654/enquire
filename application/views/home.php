	
    <section id="midarea">
    	<div class="midtop">      
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-7 col-sm-6">
                	<h2>enquirehub is a secure global business directory<br> which helps businesses connect. </h2>
                 <div class="pl20">  
                  <h3> enquirehub helps: </h3>
                  <div class="just-padding">

                    <div class="list-group list-group-root well">
                      
                      <a href="#item-1" class="list-group-item" data-toggle="collapse">
                        <i class="fa fa-info" aria-hidden="true"></i> Find business information
                      </a>
                      <div class="list-group collapse" id="item-1">    
                        <a href="#item-1-1" class="list-group-item" data-toggle="collapse">
                          enquirehub lets you search for and find business information in an easy-to-read format. All the information is presented in a standardised way, so information can be compared easily across multiple companies.
                        </a>    
                      </div>

                      <a href="#item-3" class="list-group-item" data-toggle="collapse">
                        <i class="fa fa-envelope" aria-hidden="true"></i> Directly contact businesses
                      </a>
                      <div class="list-group collapse" id="item-3">
                        <a href="#item-3-1" class="list-group-item" data-toggle="collapse">
                          On enquirehub, businesses can easily send messages to one another. You can put in an enquiry quickly. Our messaging system also lets senders know when their message has been opened and read, so you know when to expect a reply.
                        </a>
                      </div>

                      <a href="#item-4" class="list-group-item" data-toggle="collapse">
                        <i class="fa fa-eye" aria-hidden="true"></i> Increase the visibility of your business
                      </a>
                      <div class="list-group collapse" id="item-4">
                        <a href="#item-4-1" class="list-group-item" data-toggle="collapse">
                          Being on the platform allows others to quickly search for your business information. Once you fill out your key information, entering keywords relating to your company will bring your business to attention.
                        </a>
                      </div>
                      
                      <a href="#item-2" class="list-group-item" data-toggle="collapse">
                        <i class="fa fa-phone" aria-hidden="true"></i> Act as a free alternative to setting up a website
                      </a>
                      <div class="list-group collapse" id="item-2">
                        <a href="#item-2-1" class="list-group-item" data-toggle="collapse">
                          enquirehub is a free way to have an online presence without the time and effort associated with setting up your own website, which can also be complicated and hassle-some. enquirehub makes it much more simple to set up a profile.
                        </a>  
                      </div>

                      <a href="#item-5" class="list-group-item" data-toggle="collapse">
                        <i class="fa fa-link" aria-hidden="true"></i>  Create a premium backlink to your website
                      </a>
                      <div class="list-group collapse" id="item-5">
                        <a href="#item-5-1" class="list-group-item" data-toggle="collapse">
                          If you already have a website, enquirehub acts as a direct backlink to it. It adds to the SEO (search engine optimisation) and increases your website’s ranking within search engines. As enquirehub is a secure platform, you can be assured that it provides a trustworthy backlink.
                        </a>
                      </div>
                      
                      <a href="#item-6" class="list-group-item" data-toggle="collapse">  
                        <i class="fa fa-globe" aria-hidden="true"></i> Promote your business on a global scale
                      </a>  
                      <div class="list-group collapse" id="item-6">
                        <a href="#item-6-1" class="list-group-item" data-toggle="collapse">
                         By having a profile on enquirehub, you extend your company’s presence online. Being a global platform, it helps to promote possible exports. 
                        </a>
                      </div>  
                      
                    </div>
                      
                  </div>


                  
                  </div>
                  <div class="business_box">
                    <?php /*?><div class="row">
                      <div class="col-md-3"><img src="http://placehold.it/150?text=Business+Profile"></div>
                      <div class="col-md-3"><img src="http://placehold.it/150?text=Business+Profile"></div>
                      <div class="col-md-3"><img src="http://placehold.it/150?text=Business+Profile"></div>
                      <div class="col-md-3"><img src="http://placehold.it/150?text=Business+Profile"></div>
                    </div><?php */?>
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Profile Page Advert -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-3732786407521019"
     data-ad-slot="5224869088"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                  </div>
                </div>
                <div class="col-lg-5 col-sm-6">
                	<div class="sidepart">
                    	<div class="form3-header">
                        	<h3 class="fm-heading">Add your business for free to an international business directory!</h3>
                        </div>
                        <div class="form3-body">
                        	<form action="<?= base_url();?>auth/sign_up" class="formr" id="signupForm" method="post" accept-charset="utf-8">
                                <div class="form-group">
                                  <!--<div class="radio radio-info radio-inline">                                    
                                      <input type="radio" name="group_id" id="optionsRadios1" value="1" checked="">
                                      <label for="optionsRadios1">Register business </label>
                                  </div>
                                  <div class="radio radio-info radio-inline">                                        
                                      <input type="radio" name="group_id" id="optionsRadios2" value="2">
                                      <label for="optionsRadios2">I am an individual </label>
                                  </div>-->
                                </div>                                
                                <div class="form-group">  
                                   <input type="text" name="username" value="" id="username" class="form-control" placeholder="Business name">
                                </div>
                                <div class="form-group"> 
                                	<input type="text" name="email" value="" id="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group"> <input type="password" name="password" value="" id="passcord" class="form-control" placeholder="Password">
                                 </div>
                                <div class="form-group"> <input type="password" name="retype_password" value="" id="retype_password" class="form-control" placeholder="Confirm password">
                                </div>
                                <p><?php echo $form->field_recaptcha(); ?></p>
                                <div class="form-group"><input type="submit" name="submit" value="Sign up Now!" class="btn btn-default btn-block btn-lg">
                                </div>                                
                                <div class="warning">By clicking signup now, you agree to enquirehub’s <strong><a href="<?php echo base_url(); ?>page/user-agreement">User Agreement</a>, <a href="<?php echo base_url(); ?>page/privacy-policy">Privacy Policy</a>,</strong> and <strong><a href="<?php echo base_url(); ?>page/cookie-policy">Cookie Policy</a></strong>.</div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="midbottom">
        	<div class="searchbusiness">
        		<div class="container">            
            		<h1>Find A Business</h1>
                    <form action ="<?=base_url()?>home/search" method="get" id="searchform" class="form-inline form4">
                      <div class="form-group sbox">
                        <label class="sr-only" for="exampleInputEmail3">Email address</label>
                        <input type="text" class="form-control" name="searchterm" id="searchterm" value="<?=$searchterm?>" placeholder="Search for businesses">
                      </div>
                      <div class="form-group dbox">
                        <label class="sr-only" for="exampleInputPassword3">Password</label>
                        <select id="location" name="location" class="form-control">
                        	<option selected="selected" value="">Select Location</option>
                        </select>
                      </div>
                      
                      <input type="submit" class="btn btn-default" value="Search">
                    </form>
                </div>
            </div>
        </div>
    </section>