<div class="bloombox">
  <div class="ProfileMid">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 pro1">
          <h3> <?php echo $userDetails->username;?> </h3>
          <?php 
		  if($businessDetails->profile_img_path != ''){
		  	$imageUrl = base_url()."assets/uploads/profile/".$businessDetails->profile_img_path; 
		  }else{
			$imageUrl = base_url()."assets/dist/images/pro.png";  
		  }
		  ?>
          <img src="<?= $imageUrl;?>" class="img-responsive" alt="Profile Picture">
          <p><strong>Date joined :</strong> <?= date('M d, Y', $userDetails->created_on);?></p>
          <p><strong>Last updated :</strong> <?= date('M d, Y', $businessDetails->last_modified);?></p>
		      <p><strong>Last logged in :</strong> <?= date('M d, Y', $userDetails->last_login);?></p>
		      <p>Review score (to be incorporated later when final decisions have been made on how the review system will work)</p>
        </div>
        <div class="col-sm-9 pro2">
          <div class="tabs tabs-style-underline">
            <nav>
              <ul>
                <li><a href="#section-underline-1" class="icon icon-avatar"><span>Profile</span></a></li>
                <li><a href="#section-underline-2" class="icon icon-tags"><span>Products</span></a></li>
                <li><a href="#section-underline-3" class="icon fa fa-cog"><span>Services</span></a></li>
                <li><a href="#section-underline-4" class="icon icon-newspaper"><span>News</span></a></li>
                <li><a href="#section-underline-5" class="icon icon-picture"><span>Photos</span></a></li>               
                <li><a href="#section-underline-6" class="icon icon-command"><span>Contact</span></a></li>
                <li><a href="#section-underline-7" class="icon icon-embed"><span>Embed Code</span></a></li>                
              </ul>
            </nav>
            <div class="content-wrap">
              <section id="section-underline-1">
                <div class="profile_description">
                  <h3 class="name_c"><span>About</span>
                    <?= $userDetails->username;?>
                  </h3>
                  <p>
                    <?= $businessDetails->description;?>
                  </p>
                </div>
              </section>
              <section id="section-underline-2">
                <p>Here is a change</p>
              </section>
              <section id="section-underline-3">
                <p>3adsf</p>
              </section>
              <section id="section-underline-4">
                <p>4</p>
              </section>
              <section id="section-underline-5">
                <p>5</p>
              </section>
              <section id="section-underline-6">
                <p>3</p>
              </section>
              <section id="section-underline-7">
                <p>4</p>
              </section>
              <section id="section-underline-8">
                <p>5</p>
              </section>
            </div>
            <!-- /content --> 
          </div>
          <!-- /tabs --> 
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
          var geocoder;
          var map;
           var address ="<?php echo ($datasl->address.", ". $datasl->city.", ". $datasl->state.", ". $datasl->zipcode);?>";
          function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(175.397, 150.644);
            var myOptions = {
              zoom: 16,
              center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            if (geocoder) {
              geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                  map.setCenter(results[0].geometry.location);

                    var infowindow = new google.maps.InfoWindow(
                        { content: '<b>'+address+'</b>',
                          size: new google.maps.Size(150,50)
                        });

                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map, 
                        title:address
                    }); 
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });

                  } else {
                    alert("No results found");
                  }
                } else {
                  //alert("Geocode was not successful for the following reason: " + status);
                }
              });
            }
          }
        </script> 
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-GXGUh86CXYfyFqLSOb0rkvR0_W9cIto&callback=initialize">
        </script>
<div id="map_canvas" ></div>
