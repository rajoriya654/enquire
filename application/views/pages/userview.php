<div class="bloombox">
  <div class="ProfileMid">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 pro1">
          <h3> <?php echo $datasl->username;?> </h3>
          <?php 
		  if($datasl->profile_img_path != ''){
		  	$imageUrl = base_url()."assets/uploads/profile/".$datasl->profile_img_path; 
		  }else{
			$imageUrl = base_url()."assets/dist/images/pro.png";  
		  }
		  ?>
          <img src="<?= $imageUrl;?>" class="img-responsive" alt="Profile Picture">
          <p><strong>Date joined :</strong> <?= date('M d, Y', $datasl->created_on);?></p>
          <p><strong>Last updated :</strong> <?= date('M d, Y', $datasl->last_modified);?></p>
		      <p><strong>Last logged in :</strong> <?= date('M d, Y', $datasl->last_login);?></p>
		      <p>Review score (to be incorporated later when final decisions have been made on how the review system will work)</p>
        </div>
        <div class="col-sm-9 pro2">
          <div class="tabs tabs-style-underline">
            <nav>
              <ul>
                <li><a href="#section-underline-1" class="icon icon-avatar"><span>Profile</span></a></li>
                <li><a href="#section-underline-2" class="icon icon-tags"><span>Products</span></a></li>
                <li><a href="#section-underline-3" class="icon fa fa-cog"><span>Services</span></a></li>
                <li><a href="#section-underline-4" class="icon icon-newspaper"><span>News</span></a></li>
                <li><a href="#section-underline-5" class="icon icon-picture"><span>Photos</span></a></li>               
                <li><a href="#section-underline-6" class="icon icon-command"><span>Contact</span></a></li>
                <li><a href="#section-underline-7" class="icon icon-embed"><span>Embed Code</span></a></li>                
              </ul>
            </nav>
            <div class="content-wrap">
              <section id="section-underline-1">
                <div class="profile_description">
                  <h3 class="name_c"><span>About</span>
                  </h3>
                  <p>
                    <?= $datasl->description;?>
                    <!-- need to implement pulling rest of data-->
                    <br>
                   Website :  <?= $datasl->website;?>
                    <br>
                    Phone : <?= $datasl->phone;?>
                    <br>
                    Created on : <?php echo date('d/m/y',$datasl->created_on); ?>
                    <br>
                    Email : <?= $datasl->email; ?>
                    <br>
                    Last Login : <?php echo date('d/m/y',$datasl->last_login); ?> 
                    <br>
                    Category : <?= $category->category_name; ?>

                  </p>
                </div>
              </section>
              <section id="section-underline-2">
                <p>Here is a change</p>
                <?php echo($datasl->address.", ". $datasl->city.", ". $datasl->state." ". $datasl->zipcode);?>
              </section>
              <section id="section-underline-3">
                <p>3adsf</p>
              </section>
              <section id="section-underline-4">
                <p>4</p>
              </section>
              <section id="section-underline-5">
                <p>5</p>
              </section>
              <section id="section-underline-6">
                <p><h2>Contact Us</h2></p>
                <p>Telephone: <?= $datasl->phone?> </p>
                <p>Email: <?= $datasl->email?> </p>
                
              </section>
              <section id="section-underline-7">
                <p>4</p>
              </section>
            </div>
            <!-- /content --> 
          </div>
          <!-- /tabs --> 
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
          var geocoder;
          var map;
          var address ="<?php echo ($datasl->address.", ". $datasl->city.", ". $datasl->state.", ". $datasl->zipcode);?>";
          function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions = {
              zoom: 16,
              center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            if (geocoder) {
              geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                  map.setCenter(results[0].geometry.location);

                    var infowindow = new google.maps.InfoWindow(
                        { content: '<b>'+address+'</b>',
                          size: new google.maps.Size(150,50)
                        });

                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map, 
                        title:address
                    }); 
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });

                  } else {
                    alert("No results found");
                  }
                } else {
                  //alert("Geocode was not successful for the following reason: " + status);
                }
              });
            }
          }
        </script> 
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-GXGUh86CXYfyFqLSOb0rkvR0_W9cIto&callback=initialize">
        </script>
<div id="map_canvas" ></div>

<div class="midbottom">
  <div class="searchbusiness">
    <div class="container">            
        <h1>Find A Business</h1>
            <form action ="<?=base_url()?>home/search" method="get" id="searchform" class="form-inline form4">
              <div class="form-group sbox">
                <label class="sr-only" for="exampleInputEmail3">Email address</label>
                <input type="text" class="form-control" name="searchterm" id="searchterm" value="<?=$searchterm?>" placeholder="Search for businesses">
              </div>
              <div class="form-group dbox">
                <label class="sr-only" for="exampleInputPassword3">Password</label>
                <select id="location" name="location" class="form-control">
                  <option selected="selected" value="">Select Location</option>
                </select>
              </div>
              
              <input type="submit" class="btn btn-default" value="Search">
            </form>
        </div>
    </div>
</div>
