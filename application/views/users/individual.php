
<?php /* ?><h3>Welcome, <?php if($user->first_name != '') {echo $user->first_name.' '.$user->last_name;} else { echo $user->email;} ?>!</h3>

<a href="auth/logout" class="btn">Logout</a><?php */ ?>
<? // print_r($userDetails);?>

<div id="wrapper7">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h1>Become Visible In 1 Minute <small>Complete Yourself With enquirehub</small></h1>
        </div>
        <div class="account-box">
          <?php $user_id = $userDetails->id;?> 
          <?php if($this->session->flashdata('success') != ''){?>
              <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <?php echo @$this->session->flashdata('success');?>
                </div>
              </div>
          <?php }?>




          <?php $attributes = array('class' => 'forms3', 'enctype'=>'multipart/form-data'); echo form_open("profile/individual", $attributes);?>
          <?php /* ?><div class="col-md-4">
          	<div class="sidebar_account">           
            <h2>Other Information</h2>
            <hr />
            <ul>
            	<li><a href="<?= base_url();?>auth/change_email/">Change Email</a></li>
                <li><a href="<?= base_url();?>auth/change_password/">Change Password</a></li>
                <li><a href="<?= base_url();?>auth/unregister/">Unregister</a></li>
                <li><a href="<?= base_url();?>auth/logout/">Logout</a></li>
            </ul>
            </div>
          </div><?php */ ?>
          <div class="col-md-12 white_background">

          <div class="roxen-in-header">
          <h1>You are just one step away from awesomeness!</h1>
          <h2>Personal Information</h2><hr />
          </div>
          
          <?php //echo "<pre>";print_r($userDetalis); echo "</pre>";?>
          <?php if($this->session->flashdata('message') != ''){?><br />
            <div class="alert alert-danger" role="alert">
              <?php echo @$this->session->flashdata('message');?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
          <?php }?>



          <?php /* ?><h2>Hello <?php if($user->first_name != '') {echo $user->first_name.' '.$user->last_name;} else { echo $user->email;} ?>, <small>Update your profile</small></h2><?php */ ?>
          
              
              <div class="row">
                <div class="col-sm-6">   
                  <div class="form-group <?php if(form_error('firstname') != ''){?>has-error<?php } ?>">                                                                
                      <?php echo form_label('First Name', $firstname['id'], array('class'=>'label_class required')); ?>	
                      <?php echo form_input($firstname);?>
                      <?php echo form_error('firstname'); ?>                    
                  </div>
                </div>

                <div class="col-sm-6">  
                  <div class="form-group <?php if(form_error('lastname') != ''){?>has-error<?php } ?>">                                                            
                      <?php echo form_label('Last Name', $lastname['id'], array('class'=>'label_class required')); ?>	
                      <?php echo form_input($lastname);?>
                      <?php echo form_error('lastname'); ?>                  
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">              
                  <div class="form-group">                                
                      <label class="label_class">EMAIL ADDRESS</label>
                      <input class="form-control" type="text" readonly="readonly" value="<?= $userDetails->email;?>">                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group <?php if(form_error('phonenumber') != ''){?>has-error<?php } ?>">                                                    
                      <?php echo form_label('Phone Number', $phonenumber['id'], array('class'=>'label_class required')); ?>	
                      <?php echo form_input($phonenumber);?>
                      <?php echo form_error('phonenumber'); ?>                    
                  </div>
                </div>  
              </div>  


              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group <?php if(form_error('jobtitle') != ''){?>has-error<?php } ?>">                                                                  
                      <?php echo form_label('JobTitle', $jobtitle['id'], array('class'=>'label_class')); ?>
                      <?php echo form_input($jobtitle);?>
                      <?php echo form_error('jobtitle'); ?>                  
                  </div>
                </div>
                
                <div class="col-sm-6">  
                  <div class="form-group <?php if(form_error('working_at') != ''){?>has-error<?php } ?>">                                                
                      <?php echo form_label('Working At', $working_at['id'], array('class'=>'label_class')); ?>
                      <?php echo form_input($working_at);?>
                      <?php echo form_error('working_at'); ?>                
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4">    
                  <div class="form-group <?php if(form_error('experience') != ''){?>has-error<?php } ?>">                                                    
                      <?php echo form_label('Experience', $experience['id'], array('class'=>'label_class')); ?>
                      <?php echo form_input($experience);?>
                      <?php echo form_error('experience'); ?>                    
                  </div>              
                </div>
                
                <div class="col-sm-4">                
                  <div class="form-group <?php if(form_error('country') != ''){?>has-error<?php } ?>">                                                    
                      <?php echo form_label('Country', '', array('class'=>'label_class')); ?>
                      <?php echo country_dropdown('country', 'cont', 'form-control', $userDetail->country, array('US','CA','GB'), '');?>
                      <?php echo form_error('country'); ?>                    
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group <?php if(form_error('zipcode') != ''){?>has-error<?php } ?>">                                                                  
                      <?php echo form_label('zipcode', $zipcode['id'], array('class'=>'label_class')); ?>
                      <?php echo form_input($zipcode);?>
                      <?php echo form_error('zipcode'); ?>                  
                  </div>
                </div>
              </div> 

              <div class="row">
                <div class="col-sm-6">                   
                  <div class="form-group <?php if(form_error('address1') != ''){?>has-error<?php } ?>">                                                              
                      <?php echo form_label('Address# 1', $address1['id'], array('class'=>'label_class')); ?>
                      <?php echo form_input($address1);?>
                      <?php echo form_error('address1'); ?>                    
                  </div>
                </div>  

                <div class="col-sm-6">
                  <div class="form-group <?php if(form_error('address2') != ''){?>has-error<?php } ?>">                                                    
                      <?php echo form_label('Address# 2', $address2['id'], array('class'=>'label_class')); ?>
                      <?php echo form_input($address2);?>
                      <?php echo form_error('address2'); ?>                    
                  </div>
                </div>  
              </div>
              
              <hr />
              <div class="row">
              <div class="form-group <?php if(form_error('profile_img_path') != ''){?>has-error<?php } ?>">
              <div class="col-sm-8">
                  <div class="accoutn-img">
                    <!--INPUT-->
                   <?php $imageUrl = base_url()."assets/uploads/profile/".$userDetail->profile_img_path; 
                   if($userDetail->profile_img_path != ''){
                   ?>
                   <img src="<?= $imageUrl; ?>" class="img-responsive pro"><br />
                   <?php /*?><input name = "profile_img_path" type="file" class="input-xlarge custom-file-input" id = "profile_img_path" /> <?php */?>
                    <div class="box">
                      <input type="file" name="profile_img_path" id="profile_img_path" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
                      <label for="profile_img_path"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose Picture&hellip;</strong></label>
                    </div>
                   <?php }else{ ?>
                   
                    <div class="box">
                      <input type="file" name="profile_img_path" id="profile_img_path" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
                      <label for="profile_img_path"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose Picture&hellip;</strong></label>
                    </div>
                   <?php } ?>
                   <?php echo form_error('profile_img_path'); ?>
                  </div>

              </div>
              </div>
              </div>
              <hr />

              <div class="row">
              
              <div class="form-group">
                <div class="col-md-7"><input type="submit" value="Update Account" class="btn btn-success"></div>
              </div>
            
              </div>
          </div>
          <div class="clearfix"></div>
          <?php echo form_close();?>
        </div>        
      </div>      
    </div>
  </div>
</div>
